(function () {
    angular
        .module("DepartmentApp")
        .controller("SearchCtrl", SearchCtrl);

    SearchCtrl.$inject = ["$http"];

    function SearchCtrl($http) {
        var vm = this;
        vm.result={
            dept_name : 'alex'
        }
        vm.editorEnabled = false;
        vm.editableDeptName = "";

        vm.enableEditor = function () {
            vm.editorEnabled = true;
            vm.editableDeptName = vm.result.dept_name;
        };

        vm.disableEditor = function () {
            vm.editorEnabled = false;
        };

        vm.save = function () {
            vm.result.dept_name = vm.editableDeptName;

            vm.disableEditor();
        };

        vm.deleteEmpolyeeNo = function () {

        }
    };
})();






