//Load express
var express = require("express");
//Create an instance of express application
var app = express();

var bodyParser = require("body-parser");

//TODO : Import Sequelize <Here>



app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

/* Serve files from public directory
 __dirname is the absolute path of
 the application directory
 */

app.use(express.static(__dirname + "/../client/"));

//TODO :Create a connection with the MySQL DB here

 
/*var Department = require('./model/department')(sequelize, Sequelize);
var Manager = require('./model/manager')(sequelize, Sequelize);*/

//TODO : Create table relationships of manager and departments

//TODO : Create route handlers to execute sequelize queries.
//TODO: Read query string parameters
//e.g. app.get(....)


//Start the web server on port 3000
app.listen(3000, function () {
    console.info("Webserver started on port 3000");
});
